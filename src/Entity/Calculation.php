<?php

namespace App\Entity;
use Symfony\Component\Validator\Mapping\ClassMetadata;
use Symfony\Component\Validator\Constraints\NotBlank;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\CalculationRepository")
 */
class Calculation
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="float")
     */
    private $firstNumber;

    /**
     * @ORM\Column(type="string", length=1)
     */
    private $operation;

    /**
     * @ORM\Column(type="float")
     */
    private $secondNumber;

    /**
     * @ORM\Column(type="string")
     */
    private $answer;

    /**
     * @ORM\Column(type="string")
     */
    private $userMessage;    

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getFirstNumber(): ?float
    {
        return $this->firstNumber;
    }

    public function setFirstNumber(float $firstNumber): self
    {
        $this->firstNumber = $firstNumber;

        return $this;
    }

    public function getOperation(): ?string
    {
        return $this->operation;
    }

    public function setOperation(string $operation): self
    {
        $this->operation = $operation;

        return $this;
    }

    public function getSecondNumber(): ?float
    {
        return $this->secondNumber;
    }

    public function setSecondNumber(float $secondNumber): self
    {
        $this->secondNumber = $secondNumber;

        return $this;
    }

    public function getAnswer(): ?string
    {
        return $this->answer;
    }

    public function setAnswer(string $answer): self
    {
        $this->answer = $answer;

        return $this;
    }
    
    public function getUserMessage(): ?string
    {
        return $this->userMessage;
    }

    public function setUserMessage(string $userMessage): self
    {
        $this->userMessage = $userMessage;

        return $this;
    }

}
