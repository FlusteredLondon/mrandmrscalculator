<?php

namespace App\Form;

use App\Entity\Calculation;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

class CalculationType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('firstNumber', NumberType::class, [
                'attr' => [
                    'placeholder' => '?',
                    'class' => 'numberEntry'
                ]
            ])
            ->add('operation',ChoiceType::class, [
                'attr' => [
                    'class' => 'numberOperation',
                    'style' => 'width: 30px'
                   
                ],
                'choices' => [
                    '+' => 'plus',
                    '-' => 'minus',
                    '*' => 'times',
                    '/' => 'divide',
                    
                ]
            ])
            ->add('secondNumber', TextType::class, [
                'attr' => [
                    'placeholder' => '?',
                    'class' => 'numberEntry'                    
                ]
            ])
             ->add('=', SubmitType::class, [
                'attr' => [
                   'class' => 'buttonStyle',
                   'style' => 'width: 30px'
                ]
            ])
            ->add('answer', TextType::class, [
                'attr' => [
                    'readonly' => 'readonly',
                    'style' => 'border: none'
                ]
            ])
             ->add('userMessage', TextType::class, [
                'attr' => [
                    'readonly' => 'readonly',
                    'class' => 'userMessage',
                    'style' => 'border: none; width: 100%;'
                ]
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Calculation::class,
        ]);
    }
}
