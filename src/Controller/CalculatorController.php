<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Form\AbstractType;

use App\Entity\Calculation;
use App\Form\CalculationType;

class CalculatorController extends AbstractController
{
    /**
     * @Route("/calculator", name="calculator")
     */
    public function index()
    {
        
        $calculation = new Calculation();
        
        $invalidPost = false;
        $errorMessage = '';

        // check to ensure the cirrect form submitted.
        if (count($_POST)!=0 && isset($_POST['calculation'])){
 
            // check for empty values
            if (!isset($_POST['calculation']['firstNumber']) || $_POST['calculation']['firstNumber']==''){
                $invalidPost = true;
                $errorMessage .= 'First Number Missing. ';
            } elseif (!isset($_POST['calculation']['secondNumber']) || $_POST['calculation']['secondNumber']=='') {
                $invalidPost = true;
                $errorMessage .= 'Second Number Missing. ';
            }

            
            if (!$invalidPost){
                // check for numbers
                if (is_numeric($_POST['calculation']['firstNumber'])){
                    $firstNumber = floatval($_POST['calculation']['firstNumber']);
                } else {
                    $invalidPost = true;
                    $errorMessage .= 'First number is not numeric. ';
                    $firstNumber = 0;
                }
                
                if (is_numeric($_POST['calculation']['secondNumber'])){
                    $secondNumber = floatval($_POST['calculation']['secondNumber']);
                } else {
                    $invalidPost = true;
                    $errorMessage .= 'Second number is not numeric. ';
                    $secondNumber = 0;
                }
                
                $operationType = $_POST['calculation']['operation'];
                
                
                //  set the output fields
                $calculation->setFirstNumber($firstNumber);
                $calculation->setSecondNumber($secondNumber);
                $calculation->setOperation($operationType);
        
                
                // do the operation
                if (!$invalidPost){
                    switch ($operationType){
                        case 'plus':
                            $calculation->setAnswer($firstNumber + $secondNumber);
                            break;
                        case 'minus':
                            $calculation->setAnswer($firstNumber - $secondNumber);
                            break;
                        case 'times':
                            $calculation->setAnswer($firstNumber * $secondNumber);
                            break;
                        case 'divide':
                            $calculation->setAnswer($firstNumber / $secondNumber);
                            break;
                        default:
                            $invalidPost = true;
                            $errorMessage .= 'Unknown Operation. '; 
                    } 
                }
            } else {
                
                // if no data
                $invalidPost = true;
                $errorMessage .= 'Missing Data. ';
            }
            unset($_POST);
        }
        
        // mark any error in the message field.
        if ($invalidPost){
            $calculation->setUserMessage($errorMessage);
        }

        // set up the form
        $form = $this->createForm(CalculationType::class, $calculation);
        
        // remder template
        return $this->render('calculator/index.html.twig', [
            'controller_form' => $form->createView()
         ]);
    }

}
